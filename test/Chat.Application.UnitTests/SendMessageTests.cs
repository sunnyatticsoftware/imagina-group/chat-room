﻿using System;
using System.Threading.Tasks;
using Chat.Application.Commands;
using Chat.Application.UnitTests.TestSupport;
using FluentAssertions;
using Xunit;

namespace Chat.Application.UnitTests;

public static  class SendMessageTests 
{
    public class When_User_Sending_A_Message : Given_When_Then_Async
    {
        private SendMessageToChatRoomCommand _command;
        private SendMessageToChatRoomHandler _sut;
        private Exception _exception = null!;

        protected override Task Given()
        {
            var chatRoomId = Guid.NewGuid();
            var userName = "JuanNieves";
            var message = "Hola mundo";
            _command = new SendMessageToChatRoomCommand(chatRoomId,userName,message);
            _sut = new SendMessageToChatRoomHandler();
            return Task.CompletedTask;
        }

        protected override async Task When()
        {
            try
            {
                await _sut.Handle(_command);
            }
            catch (Exception e)
            {
                _exception = e;
            }
        }

        [Fact]
        public void Then_It_Should_Not_Throw_Exception()
        {
            _exception.Should().BeNull();
        }
    }
}