using System;
using System.Threading.Tasks;
using Chat.Application.Commands;
using Chat.Application.UnitTests.TestSupport;
using Chat.Domain;
using FluentAssertions;
using Moq;
using Xunit;

namespace Chat.Application.UnitTests.ChatRoomCommandTests;

public static class CreateChatRoomTests
{
    public class When_Creating_New_Chat_Room : Given_When_Then_Async
    {
        private CreateChatRoomCommand _command;
        private CreateChatRoomHandler _sut;
        private Guid _id;
        private Mock<IChatRoomRepository> _repository;

        protected override Task Given()
        {
            _command = new CreateChatRoomCommand();
            _repository = new Mock<IChatRoomRepository>();
            _sut = new CreateChatRoomHandler(_repository.Object);
            
            return Task.CompletedTask;
        }

        protected override async Task When()
        {
            _id = await _sut.Handle(_command);
        }

        [Fact]
        public void Then_It_Should_Return_A_Valid_ChatRoom_Id()
        {
            _id.Should().NotBeEmpty();
        }
        
        [Fact]
        public void Then_It_Should_Save_ChatRoom()
        {
            _repository.Verify(r => r.Save(It.Is<ChatRoom>(cr => cr.Id != Guid.Empty)), Times.Once);
        }
    }
}