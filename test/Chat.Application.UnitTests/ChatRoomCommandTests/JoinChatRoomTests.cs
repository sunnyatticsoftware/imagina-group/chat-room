using System;
using System.Threading.Tasks;
using Chat.Application.Commands;
using Chat.Application.UnitTests.TestSupport;
using Chat.Application.UnitTests.TestSupport.Extensions;
using FluentAssertions;
using Xunit;

namespace Chat.Application.UnitTests.ChatRoomCommandTests;

public static class JoinChatRoomTests
{
    public class When_Joining_A_Chat_Room : Given_When_Then_Async
    {
        private Guid _chatRoomId;
        private string _userName;
        private string _connectionId;
        private JoinChatRoomCommand _command;
        private JoinChatRoomHandler _sut;
        private Exception _exception = null!;

        protected override Task Given()
        {
            _chatRoomId = 1.ToGuid();
            _userName = "JohnDoe";
            _connectionId = "connectionId1";
            _command = new JoinChatRoomCommand(_chatRoomId, _userName, _connectionId);
            _sut = new JoinChatRoomHandler();

            return Task.CompletedTask;
        }

        protected override async Task When()
        {
            try
            {
                await _sut.Handle(_command);
            }
            catch(Exception e)
            {
                _exception = e;
            }
        }

        [Fact]
        public void Then_Is_Should_Not_Throw_Exception()
        {
            _exception.Should().BeNull();
        }
    }
}