﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Chat.Application.Query;
using Chat.Application.UnitTests.TestSupport;
using Chat.Application.UnitTests.TestSupport.Extensions;
using FluentAssertions;
using Xunit;

namespace Chat.Application.UnitTests.GetAllMessagesQueryTests;

public static class GetAllMessagesTests
{
    public class When_Getting_All_Message : Given_When_Then_Async
    {
        private GetAllMessagesQuery _query;
        private GetAllMessagesHandler _sut;
        private IEnumerable<string> _messages;
        private List<string> _expectedMessages;

        protected override Task Given()
        {
            var chatRoomId = 1.ToGuid();
            _expectedMessages = new List<string>
            {
                "$[{dateTimeFormatted}] user1 says: Hello world"
            };
            _query = new GetAllMessagesQuery(chatRoomId);
            _sut = new GetAllMessagesHandler();
            return Task.CompletedTask;
        }

        protected override async Task When()
        {
            _messages = await _sut.Handle(_query);
        }

        [Fact]
        public void Then_It_Should_Receive_All_Expected_Messages()
        {
            _messages.Should().BeEquivalentTo(_expectedMessages);
        }
    }
}