using System.Threading.Tasks;
using Xunit;

namespace Chat.Application.UnitTests.TestSupport;

public abstract class Given_When_Then_Async
    : IAsyncLifetime
{
    public async Task InitializeAsync()
    {
        await Given();
        await When();
    }
    
    protected abstract Task Given();
    protected abstract Task When();
    

    public async Task DisposeAsync()
    {
        await Cleanup();
    }

    protected virtual Task Cleanup()
    {
        return Task.CompletedTask;
    }
}