using System;
using System.Threading.Tasks;

// ReSharper disable All

namespace Chat.Domain.UnitTests.TestSupport;

public abstract class Given_When_Then
    : IDisposable
{
    protected Given_When_Then()
    {
        Setup();
    }

    private void Setup()
    {
        Given();
        When();
    }

    protected abstract void Given();
    protected abstract void When();
    
    public void Dispose()
    {
        Cleanup();
    }

    protected virtual void Cleanup()
    {
    }
}