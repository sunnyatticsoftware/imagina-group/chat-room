using System;
using Chat.Domain.UnitTests.TestSupport.Extensions;
using Moq;

namespace Chat.Domain.UnitTests.TestSupport.Builders;

public class ChatRoomBuilder
{
    private Guid _id = 1.ToGuid();
    private INotificationService _notificationService = new Mock<INotificationService>().Object;
    private IDateTimeProvider _dateTimeProvider = new Mock<IDateTimeProvider>().Object;

    public ChatRoom Object => new ChatRoom(_id, _notificationService, _dateTimeProvider);

    public ChatRoomBuilder WithId(Guid id)
    {
        _id = id;

        return this;
    }
    
    public ChatRoomBuilder WithNotificationService(INotificationService notificationService)
    {
        _notificationService = notificationService;
        
        return this;
    }

    public ChatRoomBuilder WithDateTimeProvider(IDateTimeProvider dateTimeProvider)
    {
        _dateTimeProvider = dateTimeProvider;

        return this;
    }
}