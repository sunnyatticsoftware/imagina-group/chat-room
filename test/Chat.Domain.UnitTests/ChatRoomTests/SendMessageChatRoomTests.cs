using System;
using System.Threading.Tasks;
using Chat.Domain.UnitTests.TestSupport;
using Chat.Domain.UnitTests.TestSupport.Builders;
using Chat.Domain.UnitTests.TestSupport.Extensions;
using FluentAssertions;
using Moq;
using Xunit;

namespace Chat.Domain.UnitTests.ChatRoomTests;

public static class SendMessageChatRoomTests
{
    public class When_Sending_Message_With_Existing_User_To_Chat_Room : Given_When_Then_Async
    {
        private ChatRoom _sut;
        private User _user;
        private Exception _exception;
        private Mock<INotificationService> _notificationServiceMock;
        private Mock<IDateTimeProvider> _dateTimeProviderMock;
        private string _expectedDateTime;

        protected override Task Given()
        {
            _notificationServiceMock = new Mock<INotificationService>();
            _dateTimeProviderMock = new Mock<IDateTimeProvider>();
            _dateTimeProviderMock.Setup(m => m.UtcNow).Returns(1.ToUtcNow());
            _expectedDateTime = _dateTimeProviderMock.Object.UtcNow.ToString("yyyy-MM-dd HH:mm:ss");
            _sut = new ChatRoomBuilder()
                .WithId(1.ToGuid())
                .WithNotificationService(_notificationServiceMock.Object)
                .WithDateTimeProvider(_dateTimeProviderMock.Object)
                .Object;
            _user = new User("JohnDoe", "ConnectionId");
            _sut.Join(_user);
            return Task.CompletedTask;
        }

        protected override async Task When()
        {
            try
            {
                await _sut.SendMessage(_user.Username, "hello world");
            }
            catch (Exception exception)
            {
                _exception = exception;
            }
        }

        [Fact]
        public void Then_It_Should_Not_Throw_An_Exception()
        {
            _exception.Should().BeNull();
        }
        
        [Fact]
        public void Then_It_Should_Send_Expected_Notification()
        {
            _notificationServiceMock.Verify(
                n => n.Notify(_user.ConnectionId, $"[{_expectedDateTime}] {_user.Username} says: hello world"),
                Times.Once);
        }
    }
    
    public class When_Sending_Message_With_Inexisting_User_To_Chat_Room : Given_When_Then_Async
    {
        private ChatRoom _sut;
        private User _user;
        private Exception _exception;

        protected override Task Given()
        {
            _sut = new ChatRoomBuilder()
                .WithId(1.ToGuid())
                .Object;
            _user = new User("JohnDoe", "ConnectionId");
            return Task.CompletedTask;
        }

        protected override async Task When()
        {
            try
            {
               await _sut.SendMessage(_user.Username, "hello world");
            }
            catch (Exception exception)
            {
                _exception = exception;
            }
        }

        [Fact]
        public void Then_It_Should_Not_Throw_An_Exception()
        {
            _exception.Should().NotBeNull();
        }
    }

    public class When_Sending_Message_To_All_Users : Given_When_Then_Async
    {
        private ChatRoom _sut;
        private User _user1;
        private User _user2;
        private User _user3;
        private Exception _exception;
        private Mock<INotificationService> _notificationServiceMock;
        private Mock<IDateTimeProvider> _dateTimeProviderMock;
        private string _expectedDateTime;

        protected override Task Given()
        {
            _notificationServiceMock = new Mock<INotificationService>();
            _dateTimeProviderMock = new Mock<IDateTimeProvider>();
            _dateTimeProviderMock.Setup(m => m.UtcNow).Returns(1.ToUtcNow());
            _expectedDateTime = _dateTimeProviderMock.Object.UtcNow.ToString("yyyy-MM-dd HH:mm:ss");
            _sut = new ChatRoomBuilder()
                .WithId(1.ToGuid())
                .WithNotificationService(_notificationServiceMock.Object)
                .WithDateTimeProvider(_dateTimeProviderMock.Object)
                .Object;
            _user1 = new User("JohnDoe", "ConnectionId1");
            _user2 = new User("User2", "ConnectionId2");
            _user3 = new User("User3", "ConnectionId3");
            _sut.Join(_user1);
            _sut.Join(_user2);
            _sut.Join(_user3);
            return Task.CompletedTask;
        }

        protected override async Task When()
        {
            await _sut.SendMessage(_user1.Username, "hello world");
        }

        [Theory]
        [InlineData("ConnectionId1")]
        [InlineData("ConnectionId2")]
        [InlineData("ConnectionId3")]
        public void Then_It_Should_Send_Message_To_Users(string connectionId)
        {
            _notificationServiceMock.Verify(
                n => n.Notify(connectionId, $"[{_expectedDateTime}] {_user1.Username} says: hello world"),
                Times.Once);
        }
    }
}