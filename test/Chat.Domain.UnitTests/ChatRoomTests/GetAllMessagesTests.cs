﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Chat.Domain.UnitTests.TestSupport;
using Chat.Domain.UnitTests.TestSupport.Builders;
using Chat.Domain.UnitTests.TestSupport.Extensions;
using FluentAssertions;
using Moq;
using Xunit;

namespace Chat.Domain.UnitTests.ChatRoomTests;

public static class GetAllMessagesTests
{
    public class When_Getting_Messages_From_Empty_ChatRoom : Given_When_Then_Async
    {
        private ChatRoom _sut;
        private IEnumerable<string> _messages;

        protected override Task Given()
        {
            _sut = new ChatRoomBuilder()
                .WithId(1.ToGuid()).Object;
            return Task.CompletedTask;
        }

        protected override Task When()
        {
            _messages = _sut.GetAllMessages();
            return Task.CompletedTask;
        }

        [Fact]
        public void Then_It_Should_Empty_Message()
        {
            _messages.Should().BeEmpty();
        }
    }

    public class When_Getting_Messages_From_Silent_ChatRoom : Given_When_Then_Async
    {
        private ChatRoom _sut;
        private IEnumerable<string> _messages;
        private User _user1;

        protected override Task Given()
        {
            _sut = new ChatRoomBuilder()
                .WithId(1.ToGuid()).Object;
            _user1 = new User("UserName","connectionId");
            _sut.Join(_user1);
            return Task.CompletedTask;
        }

        protected override Task When()
        {
            _messages = _sut.GetAllMessages();
            return Task.CompletedTask;
        }
        
        [Fact]
        public void Then_It_Should_Empty_Message()
        {
            _messages.Should().BeEmpty();
        }
    }
    
    public class When_Getting_Messages_From_A_Chatty_ChatRoom : Given_When_Then_Async
    {
        private ChatRoom _sut;
        private IEnumerable<string> _messages;
        private string _expectedMessage;

        protected override async Task Given()
        {
            var user1 = new User("UserName","connectionId");
            var user2 = new User("UserName2","connectionId2");
            
            var dateTimeProviderMock = new Mock<IDateTimeProvider>();
            dateTimeProviderMock.Setup(m => m.UtcNow).Returns(1.ToUtcNow());
            var expectedDateTime = dateTimeProviderMock.Object.UtcNow.ToString("yyyy-MM-dd HH:mm:ss");
            
            _expectedMessage = $"[{expectedDateTime}] {user1.Username} says: hello world";
            _sut = new ChatRoomBuilder()
                .WithId(1.ToGuid())
                .WithDateTimeProvider(dateTimeProviderMock.Object).Object;
            
            _sut.Join(user1);
            _sut.Join(user2);
            
            await _sut.SendMessage(user1.Username, "hello world");
        }

        protected override Task When()
        {
            _messages = _sut.GetAllMessages();
            return Task.CompletedTask;
        }
        
        [Fact]
        public void Then_It_Should_Not_Empty_Message()
        {
            _messages.Should().NotBeEmpty();
        }
        
        [Fact]
        public void Then_It_Should_Return_Expected_Message()
        {
            _messages.Single().Should().Be(_expectedMessage);
        }
    }
}