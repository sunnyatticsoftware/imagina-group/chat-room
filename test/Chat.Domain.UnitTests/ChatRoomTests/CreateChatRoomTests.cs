using Chat.Domain.UnitTests.TestSupport;
using Chat.Domain.UnitTests.TestSupport.Extensions;
using FluentAssertions;
using System;
using Chat.Domain.UnitTests.TestSupport.Builders;
using Xunit;

namespace Chat.Domain.UnitTests.ChatRoomTests;

public static class CreateChatRoomTests
{
    public class When_Creating_Chat_Room_With_Valid_Id
        : Given_When_Then
    {
        private ChatRoom _sut = null!;
        private Guid _id;

        protected override void Given()
        {
            _id = 1.ToGuid();
        }

        protected override void When()
        {
            _sut = new ChatRoomBuilder().WithId(_id).Object;
        }

        [Fact]
        public void Then_It_Should_Have_A_Valid_Instance()
        {
            _sut.Should().NotBeNull();
        }

        [Fact]
        public void Then_It_Should_Have_A_Valid_Id()
        {
            _sut.Id.Should().NotBeEmpty();
        }

        [Fact]
        public void Then_It_Should_Have_The_Expected_Id()
        {
            _sut.Id.Should().Be(_id);
        }
    }
    
    public class When_Creating_Chat_Room_With_Invalid_Id
        : Given_When_Then
    {
        private ChatRoom _sut = null!;
        private ArgumentException _exception = null!;

        protected override void Given()
        {
        }

        protected override void When()
        {
            try
            {
                _sut = new ChatRoomBuilder().WithId(Guid.Empty).Object;
            }
            catch (ArgumentException exception)
            {
                _exception = exception;
            }
        }

        [Fact]
        public void Then_It_Should_Not_Have_A_Valid_Instance()
        {
            _sut.Should().BeNull();
        }

        [Fact]
        public void Then_It_Should_Throw_ArgumentException()
        {
            _exception.Should().NotBeNull();
        }
    }
}