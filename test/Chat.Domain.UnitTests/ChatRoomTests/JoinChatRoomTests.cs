using System;
using Chat.Domain.UnitTests.TestSupport;
using Chat.Domain.UnitTests.TestSupport.Builders;
using Chat.Domain.UnitTests.TestSupport.Extensions;
using FluentAssertions;
using Xunit;

namespace Chat.Domain.UnitTests.ChatRoomTests;

public static class JoinChatRoomTests
{
    public class When_Joining_User : Given_When_Then
    {
        private ChatRoom _sut = null!;
        private User _user = null!;
        private Exception _exception = null!;

        protected override void Given()
        {
            _sut = new ChatRoomBuilder().WithId(1.ToGuid()).Object;
            _user = new User("UserName","connectionId");
        }

        protected override void When()
        {
            try
            {
                _sut.Join(_user);
            }
            catch (Exception exception)
            {
                _exception = exception;
            }
        }

        [Fact]
        public void Then_It_Should_Not_Throw_Exception()
        {
            _exception.Should().BeNull();
        }
    }
    
    public class When_Joining_Existing_User : Given_When_Then
    {
        private ChatRoom _sut = null!;
        private User _user = null!;
        private ArgumentException _exception = null!;

        protected override void Given()
        {
            _sut = new ChatRoomBuilder().WithId(1.ToGuid()).Object;
            _user = new User("UserName","connectionId");
            _sut.Join(_user);
        }

        protected override void When()
        {
            try
            {
                _sut.Join(_user);
            }
            catch (ArgumentException exception)
            {
                _exception = exception;
            }
        }

        [Fact]
        public void Then_It_Should_Throw_Argument_Exception()
        {
            _exception.Should().NotBeNull();
        }
    }
    
    public class When_Joining_User_With_Same_UserName_And_ConnectionId : Given_When_Then
    {
        private ChatRoom _sut = null!;
        private User _user1 = null!;
        private User _user2 = null!;
        private ArgumentException _exception = null!;

        protected override void Given()
        {
            _sut = new ChatRoomBuilder().WithId(1.ToGuid()).Object;
            _user1 = new User("UserName","connectionId");
            _user2 = new User("UserName", "connectionId");
            _sut.Join(_user1);
        }

        protected override void When()
        {
            try
            {
                _sut.Join(_user2);
            }
            catch (ArgumentException exception)
            {
                _exception = exception;
            }
        }

        [Fact]
        public void Then_It_Should_Throw_Argument_Exception()
        {
            _exception.Should().NotBeNull();
        }
    }
    
    public class When_Joining_User_With_Same_UserName_But_Different_ConnectionId : Given_When_Then
    {
        private ChatRoom _sut = null!;
        private User _user1 = null!;
        private User _user2 = null!;
        private Exception _exception = null!;

        protected override void Given()
        {
            _sut = new ChatRoomBuilder().WithId(1.ToGuid()).Object;
            _user1 = new User("UserName","connectionId");
            _user2 = new User("UserName", "connectionId2");
            _sut.Join(_user1);
        }

        protected override void When()
        {
            try
            {
                _sut.Join(_user2);
            }
            catch (Exception exception)
            {
                _exception = exception;
            }
        }

        [Fact]
        public void Then_It_Should_Not_Throw_Exception()
        {
            _exception.Should().BeNull();
        }
    }
}