using Chat.Domain;

namespace Chat.Application.Commands;

public class CreateChatRoomHandler
{
    private readonly IChatRoomRepository _repository;

    public CreateChatRoomHandler(IChatRoomRepository repository)
    {
        _repository = repository;
    }
    
    public async Task<Guid> Handle(CreateChatRoomCommand command)
    {
        var id = Guid.NewGuid();
        var chatRoom = new ChatRoom(id, null, null);
        await _repository.Save(chatRoom);

        return id;
    }
}