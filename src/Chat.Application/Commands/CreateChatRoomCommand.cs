namespace Chat.Application.Commands;

public record CreateChatRoomCommand;