﻿namespace Chat.Application.Commands;

public record JoinChatRoomCommand(Guid ChatRoomId, string UserName, string ConnectionId);