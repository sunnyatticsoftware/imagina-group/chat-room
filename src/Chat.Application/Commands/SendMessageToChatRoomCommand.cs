﻿namespace Chat.Application.Commands;

public record SendMessageToChatRoomCommand(Guid ChatRoomId,string UserName, string Message);