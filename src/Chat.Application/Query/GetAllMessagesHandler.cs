﻿namespace Chat.Application.Query;

public class GetAllMessagesHandler
{
    public async Task<IEnumerable<string>> Handle(GetAllMessagesQuery getAllMessagesQuery)
    {
        return Enumerable.Empty<string>();
    }
}