﻿namespace Chat.Application.Query;

public record GetAllMessagesQuery(Guid ChatRoomId);