namespace Chat.Domain;

public interface IDateTimeProvider
{
    DateTime UtcNow { get; }
}