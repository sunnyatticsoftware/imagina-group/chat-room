namespace Chat.Domain;

public class ChatRoom
{
    private readonly INotificationService _notificationService;
    private readonly IDateTimeProvider _dateTimeProvider;
    private readonly List<User> _users = new List<User>();
    private readonly List<string> _messageHistory = new List<string>();
    public Guid Id { get; }

    public ChatRoom(Guid id, INotificationService notificationService, IDateTimeProvider dateTimeProvider)
    {
        if (id == Guid.Empty)
        {
            throw new ArgumentException($"Invalid Id {id}");
        }

        _notificationService = notificationService;
        _dateTimeProvider = dateTimeProvider;
        Id = id;
    }

    public void Join(User user)
    {
        if (_users.Contains(user))
        {
            throw new ArgumentException("Already added user");
        }
        
        _users.Add(user);
    }

    public async Task SendMessage(string userName, string message)
    {
        var sender = _users.FirstOrDefault(u => u.Username == userName);
        if (sender == default)
        {
            throw new ArgumentException($"User {userName} not in the chat room.");
        }

        var dateTimeFormatted = _dateTimeProvider.UtcNow.ToString("yyyy-MM-dd HH:mm:ss");
        var formattedMessage = $"[{dateTimeFormatted}] {userName} says: {message}";

        _messageHistory.Add(formattedMessage);
        
        foreach (var user in _users)
        {
            await _notificationService.Notify(user.ConnectionId, formattedMessage);
        }
    }

    public IEnumerable<string> GetAllMessages()
    {
        return _messageHistory.ToList();
    }
}