namespace Chat.Domain;

public record User(string Username, string ConnectionId);