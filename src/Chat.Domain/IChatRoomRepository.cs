namespace Chat.Domain;

public interface IChatRoomRepository
{
    Task Save(ChatRoom chatRoom);
}