namespace Chat.Domain;

public interface INotificationService
{
    Task Notify(string connectionId, string message);
}